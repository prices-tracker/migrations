### Migrations
To initialize migrations library:
```bash
go install gitlab.com/prices-tracker/migrations
```
For proper work need to set following env variables:
```
MYSQL_USER
MYSQL_PASS
MYSQL_HOST
MYSQL_PORT
MYSQL_DBNAME
```
To initialize database use:
```bash
migrations init
```
To generate migration template use (file will appear in folder db/migrations):
```bash
migrations generate name_migration
```
To run all not used migrations use:
```bash
migrations up
```
To run certain amount migrations use:
```bash
migrations up 2
```