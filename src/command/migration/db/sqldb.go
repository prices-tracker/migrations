package db

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/prices-tracker/migrations/src/command/migration/config"
	"log"
)

var DB *sql.DB

func ConnectDb() {
	log.Println("Connecting to MySQL database...")

	db, err := sql.Open("mysql", fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s?multiStatements=true",
		config.DB_USER,
		config.DB_PASS,
		config.DB_HOST,
		config.DB_PORT,
		config.DB_NAME,
	))

	if err != nil {
		log.Fatal("Unable to connect to database. Try run 'init' command. ", err.Error())
	}

	if err := db.Ping(); err != nil {
		log.Fatal("Unable to connect to database. Try run 'init' command. ", err.Error())
	}

	log.Println("Database connected")

	DB = db
}

func ConnectMysql() *sql.DB {
	db, err := sql.Open("mysql", fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/",
		config.DB_USER,
		config.DB_PASS,
		config.DB_HOST,
		config.DB_PORT,
	))

	if err != nil {
		log.Fatal("Unable to connect to mysql. Error: " + err.Error())
	}

	return db
}
